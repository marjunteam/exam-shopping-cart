/* Shopping Class */
var ShoppingCart = /** @class */ (function () {

    var currentItemFromMyCart = [];
    var totalAmount = 0;
    function ShoppingCart() {}

    /* Add to Cart */
    ShoppingCart.prototype.addToCart = function (code, name, price, quantity) {
        var myCartItems = (JSON.parse(localStorage.getItem('shopping-cart')) != null) ? JSON.parse(localStorage.getItem('shopping-cart')).items : [];
        var promoCode = $("#promo-" + code).val();
        var resultPromo = this.addPromoCodePerItem(code, promoCode, price);
            
        var itemToBeAdded = {
            id: (myCartItems.length > 0) ? myCartItems.length + 1 : 1,
            product_code: code,
            product_name: name,
            price: resultPromo.price,
            original_price: price,
            promo_code: (resultPromo.status) ? promoCode : '',
            quantity: quantity
        };

        myCartItems.push(itemToBeAdded);
        return localStorage.setItem('shopping-cart', JSON.stringify({items: myCartItems}));

    };

    /* Add to Cart PROMO */
    ShoppingCart.prototype.addToCartPromo = function (code, name, price, quantity, promo_id, promo) {
        
        var items = (JSON.parse(localStorage.getItem('shopping-cart')) != null) ? JSON.parse(localStorage.getItem('shopping-cart')).items : [];
        var free_item = '';
        var promo_price = '';
        
        if(promo_id == '1'){
            promo_price = (price * quantity) - price;
        }else if(promo_id == 2){
            promo_price = price;
        }else if(promo_id == 3){
            promo_price = price;
            var currQuantity = $("#promo-" + promo_id).val();
            if(currQuantity > 3){
                quantity = currQuantity;
                promo_price = Number(39.90 * quantity).toFixed(2);
            }else{
                alert('Quantity should be more than 3');
                return false;
            }
        }
        
        var itemToBeAdded = {
            id: (items.length > 0) ? items.length + 1 : 1,
            product_code: code,
            product_name: name,
            price: promo_price,
            original_price: price,
            promo_id: promo_id,
            free_product: free_item,
            quantity: quantity
        };

        items.push(itemToBeAdded);
        localStorage.setItem('shopping-cart', JSON.stringify({items: items}));
    }

    /* Get Total Amount */
    ShoppingCart.prototype.getTotalAmount = function () {
        var items =  (JSON.parse(localStorage.getItem('shopping-cart')) != null) ? JSON.parse(localStorage.getItem('shopping-cart')).items : [];
        var priceArray = [];
        items.forEach(function (el) {
            priceArray.push(Number(el.price).toFixed(2));
        });
        return this.sum(priceArray);
    }

    /* Calculate All Prices */
    ShoppingCart.prototype.sum = function (arrayPrice) {
        var result = 0;
        arrayPrice.forEach(function (o) { result += Number(o); });
        return result;
    };

    /* Display All Item to Cart */
    ShoppingCart.prototype.viewAddedItemToCart = function () {

        var items = (JSON.parse(localStorage.getItem('shopping-cart')) != null) ? JSON.parse(localStorage.getItem('shopping-cart')).items : [];
        var tableCart = '<table>';
            tableCart += '<tr>' +
            '<th>#</th>' +
            '<th>Item</th>' +
            '<th>Quantity</th>' +
            '<th>Price</th>' +
            '<th></th>' +
        '</tr>';

        items.forEach(function (el) {
            var finalPrice = Number(el.price).toFixed(2); 

            if(el.promo_id == 1) {
                finalPrice = Number(finalPrice).toFixed(2) + ' <span style="font-size:10px">Promo #: ' + el.promo_id + ' <br/> Pay the price of 2 only</span>';
            } else if(el.promo_id == 2){
                finalPrice = finalPrice + ' <span style="font-size:10px">Promo #: ' + el.promo_id + '<br/> free 1 GB Data-pack </span> ';
            }else if(el.promo_id == 3){
                finalPrice = finalPrice + ' <span style="font-size:10px">Promo #: ' + el.promo_id + '<br/> buys more than 3 and the price will drop to $39.90 </span> ';
            }

            var promoApplied = (el.promo_code) ? '10% off <br/> Promo Code Applied: ' + el.promo_code : '';
            tableCart += '<tr id="item-' + el.id + '">';
            tableCart += '<td>' + el.id + '</td>';
            tableCart += '<td>' + el.product_name + '</td>';
            tableCart += '<td> x' + el.quantity + '</td>';
            tableCart += '<td> $' + finalPrice +  ' <span style="font-size:10px"> ' + promoApplied + '</span></td>';
            tableCart += '<td><button onclick="removeItem(' + el.id + ')"> Remove </button></td>';
            tableCart += '</tr>'
        });

        tableCart += '</table>';
        $("#list-item-on-cart").html(tableCart);
        $("#item-cart-count").text(items.length);
        $("#total-amount").text(' $' + Number(this.getTotalAmount()).toFixed(2));
    }

    /* Remove Item on Cart */
    ShoppingCart.prototype.removeItem = function (id) {
        var items = (JSON.parse(localStorage.getItem('shopping-cart')) != null) ? JSON.parse(localStorage.getItem('shopping-cart')).items : [];
        var filteredItems = items.filter(item => item.id !== id);
        localStorage.setItem('shopping-cart', JSON.stringify({items: filteredItems}));
        this.viewAddedItemToCart();
    }

    /* Add Promo Code per Item */
    ShoppingCart.prototype.addPromoCodePerItem = function(id, code, price){
        if(code){
            if(code == 'I<3AMAYSIM'){ 
                $("#promo-" + id).val('');
                return { status: true, price: Number(price - (price * .10)).toFixed(2) };
            }else{
                alert('Invalid Promo Code');
                $("#promo-" + id).val('');
                return { status: false, price: price };
            }
        }else{
            return { status: true, price: price };
        }
    }

    /* Add Promo Code for Total Amount */
    ShoppingCart.prototype.addPromoCode = function (id) {
        var totalAmount = this.getTotalAmount();
        var promo_code = $("#code").val();
        if(promo_code == 'I<3AMAYSIM'){
            var result = Number(totalAmount - (totalAmount * .10)).toFixed(2);
            $("#discounted-amount").html('%10 Discount <br/> Discounted Amount: $' + result);
            
        }else{
            alert('Invalid Promo Code');
        }
        
    }

    return ShoppingCart;
}());