var items = new Items();
var cart = new ShoppingCart();

/* Initialize */
items.viewListItems();
items.promo();
cart.viewAddedItemToCart();

function addCart(code, name, price){
    var quantity = $('#quantity-' + code).val();
    var promoCode = $('#promo-' + code).val();
    cart.addToCart(code, name, price, quantity);
    cart.viewAddedItemToCart();
}

function addPromoCart(code, name, price, quantity, promo_id, promo){
    cart.addToCartPromo(code, name, price, quantity, promo_id, promo);
    cart.viewAddedItemToCart();
}

function removeItem(id){
    cart.removeItem(id);
    $("#item-" + id).hide();
}

function addCode(){
    cart.addPromoCode();
}