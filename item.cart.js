/* Item Class */
var Items = /** @class */ (function () {
    function Items() {
        this.items = [
            {
                product_code: 'ult_small',
                product_name: 'Unlimited 1GB',
                price: '24.90'
            },
            {
                product_code: 'ult_medium',
                product_name: 'Unlimited 2GB',
                price: '29.90'
            },
            {
                product_code: 'ult_large',
                product_name: 'Unlimited 5GB',
                price: '44.90'
            },
            {
                product_code: '1gb',
                product_name: '1 GB Data-pack',
                price: '9.90'
            }
        ];

        this.promoItem = [
            {
                id: 1,
                product_code: 'ult_small',
                product_name: 'Unlimited 1GB',
                price: '24.90',
                promo_required_quantity: '3',
                promo: '24.90',
                promo_description: 'buy 3 Unlimited 1GB Sims, pay the price of 2 only for the first month.'
            },
            {
                id: 2,
                product_code: 'ult_medium',
                product_name: 'Unlimited 2GB',
                price: '29.90',
                promo_required_quantity: '1',
                promo_description: 'free 1 GB Data-pack free-of-charge with every Unlimited 2GB sold.',
                free_product: {
                    product_code: '1gb',
                    product_name: '1 GB Data-pack',
                    price: '9.90'
                }
            },
            {
                id: 3,
                product_code: 'ult_large',
                product_name: 'Unlimited 5GB',
                price: '44.90',
                promo_required_quantity: '',
                promo: '39.90',
                promo_description: 'buys more than 3 and the price will drop to $39.90 each for the first month,'
            }
        ];
    }

    /* Promo Products */
    Items.prototype.promo = function () {
        var table = '<table>';
            table += '<tr>' +
                '<th>Promo #</th>' + 
                '<th>Product Name</th>' + 
                '<th>Description</th>' +
                '<th>Price</th>' +
                '<th>Quantity</th>' +
                '<th></th>' +
            '</tr>';

        this.promoItem.forEach(function (el) {
            var quantity = el.promo_required_quantity;
            if(el.id == 3){
                quantity = '<input style="width:30px" type="text" id="promo-'+ el.id +'" value="4"/>';
            }

            table += '<tr id="' + el.product_code + '">';
            table += '<td>' + el.id + '</td>';
            table += '<td>' + el.product_name + '</td>';
            table += '<td>' + el.promo_description + '</td>';
            table += '<td> $' + el.price + '</td>';
            table += '<td> x' + quantity + '</td>'; 
            table += `<td><button onclick="addPromoCart('${el.product_code}','${el.product_name}','${el.price}','${el.promo_required_quantity}','${el.id}','promo')"> Add to Cart </button></td>`;
            table += '</tr>'
        });
            
        table += '</table>';
        $("#promo-items").html(table);
        return true;
    }

    /* View List of Items */
    Items.prototype.viewListItems = function () {

        var table = '<table>';
        table += '<tr>' +
            '<th>Product Code</th>' +
            '<th>Product Name</th>' + 
            '<th>Quantity</th>' + 
            '<th>Price</th>' +
            '<th>Promo Code</th>' +
            '<th></th>' +
        '</tr>';

        this.items.forEach(function (el) {
            table += '<tr id="' + el.product_code + '">';
            table += '<td>' + el.product_code + '</td>';
            table += '<td>' + el.product_name + '</td>';
            table += '<td><input type="number" id="quantity-' + el.product_code  + '" name="quantity" value="1" style="width:50px"/></td>';
            table += '<td> $' + el.price + '</td>';
            table += '<td><input type="text" id="promo-' + el.product_code  + '"/></td>';
            table += `<td><button onclick="addCart('${el.product_code}','${el.product_name}','${el.price}')"> Add to Cart </button></td>`;
            table += '</tr>'
            });
            table += '</table>';
            
            $("#item-list").html(table);
                return this.items;
        };
        return Items;
}());